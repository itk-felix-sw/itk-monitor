# How to run the examples

  

1. Setup your environment
Source the itk-felix-sw setup script. 
```
source /home/itkpix/itk-felix-sw/setup.sh
  ```

2. Start the partition
Start two IPC servers. One of them is used to run the general partition and another implements the named partition to be used by the IS examples.
```
ipc_server &
```
```
ipc_server -p itkpix &
```

3. Start the IS server
Now run the is_server application:
```
is_server -p itkpix -n itkparams &
```

4. Setup the IS meta information repository (this step is not really needed)
Start the rdb_server with the XML files which contain description of your IS types:
```
rdb_server -p itkpix -d ISRepository -S /home/itkpix/itk-felix-sw/itk-monitor/schema/ITk.schema.xml
```

5. Run the Information writer
```
itk-monitor-writer -p itkpix -a itkparams.Resource1 -s Running
```

6. Run the Information reader
```
itk-monitor-reader -p itkpix -a itkparams.Resource1
```
