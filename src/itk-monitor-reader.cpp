#include <cmdl/cmdargs.h>
#include <is/info.h>
#include <is/infodictionary.h>
#include <itkdal/ITkResource.h>
#include <string>
#include <iostream>

using namespace std;

int main (int argc, char **argv){

    CmdArgBool    cVerbose   ('v', "verbose", "turn on 'verbose' mode.");
    CmdArgStr     cPartition ('p', "partition", "partition-name", 
			                       "Name of the partition. Default $TDAQ_PARTITION.");
    CmdArgStr     cAddress   ('a', "adddress", "is-address", "IS address");

    CmdLine cmd (*argv, &cVerbose, &cPartition,&cAddress,NULL);
    CmdArgvIter  arg_iter(argc-1, argv+1);
    cmd.parse(arg_iter);

    IPCCore::init (argc, argv);

    if(cVerbose) cout << "Create IPC partition" << endl;
    string partition((cPartition.flags() &  CmdArg::GIVEN?cPartition:getenv("TDAQ_PARTITION")));
    IPCPartition part(partition);

    if(cVerbose) cout << "Create IS dictionary" << endl;
    ISInfoDictionary is_server(part);
    string address(cAddress);

    if(cVerbose) cout << "Read from IS" << endl;
    ITkResource info;
    is_server.getValue(address,info);

    cout << address << ": " << info.StateMachine << endl;

    if(cVerbose) cout << "Have a nice day" << endl;
	return 0;
}
