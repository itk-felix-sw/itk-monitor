#include "itk-monitor/ItkMonitor.h"
#include <itkdal/ITkResource.h>
#include <itkdal/ITkSPchain.h>
#include <itkdal/ITkModule.h>
#include <itkdal/ITkChip.h>
#include <is/infodictionary.h>
#include <iostream>

using namespace std;

ItkMonitor * ItkMonitor::m_me=0;

ItkMonitor::ItkMonitor(){
  if(m_me==0){ m_me = this; }
  int n=0;
  IPCCore::init(n,NULL);
  m_ipc = 0;
  m_is = 0;
}

ItkMonitor::~ItkMonitor(){
  for(auto it : m_objs){
    cout << "Delete pointer: " << it.first << endl;
    delete it.second;
  }
  cout << "Disconnect from IS" << endl;
  if(m_is) delete m_is;
  cout << "Delete the IPC pointer" << endl;
  if(m_ipc) delete m_ipc;
}

ItkMonitor * ItkMonitor::GetInstance(){
  if(m_me==0){ m_me = new ItkMonitor();}
  return m_me;
}

void ItkMonitor::SetPartition(std::string partition){
  m_partition = partition;
  m_ipc = new IPCPartition(m_partition);
  m_is = new ISInfoDictionary(*m_ipc);
}

void ItkMonitor::Publish(string address, ISInfo* obj){
  if(!m_is->contains(address)){
    m_is->insert(address, *obj);
  }else{
    m_is->update(address, *obj);
  }
}

void ItkMonitor::Extract(string address, ISInfo* obj){
  if(!m_is->contains(address)){
    m_is->checkin(address, *obj);
  }else{
    m_is->getValue(address, *obj);
  }
}

ISInfo * ItkMonitor::Find(string address, string classname){
  map<string, ISInfo*>::iterator it=m_objs.begin();
  for(;it!=m_objs.end();it++){
    if(it->first==address && it->second->type().name()==classname){
      return it->second;
    }
  }
  return NULL;
}

void ItkMonitor::SetResource(string address,
			     string statemachine){

  //Find the object in memory
  ITkResource * res=(ITkResource*)Find(address,"ITkResource");
  //Create and register if not found
  if(res==NULL) { res=new ITkResource(); m_objs[address]=res; }
  //Update object contents
  res->StateMachine = statemachine;
  //Write to IS
  Publish(address,res);

}

ITkResource * ItkMonitor::GetResource(string address){

  //Find the object in memory
  ITkResource * res=(ITkResource*)Find(address,"ITkResource");
  //Create and register if not found
  if(res==NULL) { res=new ITkResource(); m_objs[address]=res; }
  //Read from IS
  Extract(address,res);
  return res;

}

void ItkMonitor::SetSPchain(string address,
			    string state, string lv, string hv, string opto, string tilock){

  //Find the object in memory
  ITkSPchain * chain=(ITkSPchain*)Find(address,"ITkSPchain");
  //Create and register if not found
  if(chain==NULL) { chain=new ITkSPchain(); m_objs[address]=chain;}
  //Update object contents
  bool update=false;
  if(chain->state != state){update=true; chain->state=state;}
  if(chain->LV != lv){update=true; chain->LV=lv;}
  if(chain->HV != hv){update=true; chain->HV=hv;}
  if(chain->Opto != opto){update=true; chain->Opto=opto;}
  if(chain->Tilock != tilock){update=true; chain->Tilock=tilock;}
  //Write to IS
  if(!update) return;
  Publish(address,chain);
}

ITkSPchain * ItkMonitor::GetSPchain(string address){

  //Find the object in memory
  ITkSPchain * chain=(ITkSPchain*)Find(address,"ITkSPchain");
  //Create and register if not found
  if(chain==NULL) { chain=new ITkSPchain(); m_objs[address]=chain; }
  //Read from IS
  Extract(address,chain);
  return chain;
  
}

void ItkMonitor::SetModule(string address, 
			   string runType){
  // Find the object in memory
  ITkModule * module = (ITkModule*)Find(address, "ITkModule");
  //Create and register if not found
  if(module == NULL) { module = new ITkModule(); m_objs[address] = module;}
  //Update object contents
  bool update = false;
  if(module -> runType != runType){update = true; module -> runType = runType;}
  //Write to IS
  if(!update) return;
  Publish(address, module);
}

ITkModule * ItkMonitor::GetModule(string address){

  //Find the object in memory
  ITkModule * module=(ITkModule*)Find(address,"ITkModule");
  //Create and register if not found
  if(module==NULL) { module=new ITkModule(); m_objs[address]=module; }
  //Read from IS
  Extract(address,module);
  return module;
  
}

void ItkMonitor::SetChip(string address, 
			 string runStatus, double eventsRate){

  // Find the object in memory
  ITkChip * chip = (ITkChip*)Find(address, "ITkChip");
  //Create and register if not found
  if(chip == NULL) { chip = new ITkChip(); m_objs[address] = chip;}
  //Update object contents
  bool update = false;
  if(chip -> runStatus != runStatus){update = true; chip -> runStatus = runStatus;}
  if(chip -> eventsRate != eventsRate){update = true; chip -> eventsRate = eventsRate;}

  //Write to IS
  if(!update) return;
  Publish(address, chip);
}

ITkChip * ItkMonitor::GetChip(string address){

  //Find the object in memory
  ITkChip * chip=(ITkChip*)Find(address,"ITkChip");
  //Create and register if not found
  if(chip==NULL) { chip=new ITkChip(); m_objs[address]=chip; }
  //Read from IS
  Extract(address,chip);
  return chip;
  
}

/*
void ItkMonitor::SetModuleDAQ(string address, 
			      string runType, string chip1runStatus, string chip2runStatus, string chip3runStatus, string chip4runStatus){

  // Find the object in memory
  ITkModuleDAQ * moduleDAQ = (ITkModuleDAQ*)Find(address, "ITkModuleDAQ");
  //Create and register if not found
  if(moduleDAQ == NULL) { moduleDAQ = new ITkModuleDAQ(); m_objs[address] = moduleDAQ;}
  //Update object contents
  bool update = false;
  if(moduleDAQ -> runType != runType)               {update = true; moduleDAQ -> runType = runType;}
  if(moduleDAQ -> chip1runStatus != chip1runStatus){update = true; moduleDAQ -> chip1runStatus = chip1runStatus;}
  if(moduleDAQ -> chip2runStatus != chip2runStatus){update = true; moduleDAQ -> chip2runStatus = chip2runStatus;}
  if(moduleDAQ -> chip3runStatus != chip3runStatus){update = true; moduleDAQ -> chip3runStatus = chip3runStatus;}
  if(moduleDAQ -> chip4runStatus != chip4runStatus){update = true; moduleDAQ -> chip4runStatus = chip4runStatus;}

  //Write to IS
  if(!update) return;
  Publish(address, moduleDAQ);
}

ITkModuleDAQ * ItkMonitor::GetModuleDAQ(string address){

  //Find the object in memory
  ITkModuleDAQ * moduleDAQ=(ITkModuleDAQ*)Find(address,"ITkModuleDAQ");
  //Create and register if not found
  if(moduleDAQ==NULL) { moduleDAQ=new ITkModuleDAQ(); m_objs[address]=moduleDAQ; }
  //Read from IS
  Extract(address,moduleDAQ);
  return moduleDAQ;
  
}
*/
