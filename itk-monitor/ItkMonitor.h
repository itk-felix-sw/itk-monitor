#ifndef ITK_MONITOR
#define ITK_MONITOR

#include <map>
#include <string>

class ISInfoDictionary;
class IPCPartition;
class ITkResource;
class ITkSPchain;
class ITkModule;
class ITkChip;
class ISInfo;

/**
 * Tool to connect to IS and get an ITKResource from it
 * \brief Read IS values
 * \author Alessandra.Palazzo@cern.ch
 * \author Enrico.Junior.Schioppa@cern.ch
 * \author Carlos.Solans@cern.ch
 * \date March 2021
 **/
class ItkMonitor{

    public:

    ItkMonitor();

    ~ItkMonitor();

    ItkMonitor * GetInstance();

    void SetPartition(std::string partition);
    
    /**
     * Write to the IS server
     * @param address the address where to write
     * @param obj the object to store 
     **/
    void Publish(std::string address, ISInfo* obj);

    /**
     * Read from the IS server
     * @param address the address where to read
     * @param obj the object to read into
     **/
    void Extract(std::string address, ISInfo* obj);

    /**
     * Find the object in the memory
     * @param address the address of the object
     * @param classname the type of the object
     * @return the generic ISInfo object
     **/
    ISInfo * Find(std::string address, std::string classname);

    void SetResource(std::string address,
		     std::string statemachine);

    ITkResource * GetResource(std::string address);

    void SetSPchain(std::string address,
		    std::string state, std::string lv, std::string hv, std::string opto, std::string tilock);

    ITkSPchain * GetSPchain(std::string address);

    void SetModule(std::string address,
		   std::string runType);

    ITkModule * GetModule(std::string address);

    /*
    void SetModuleDAQ(std::string address,
		   std::string runType, std::string chip1runStatus, std::string chip2runStatus, std::string chip3runStatus, std::string chip4runStatus);

    ITkModuleDAQ * GetModuleDAQ(std::string address);
    */

    void SetChip(std::string address,
		 std::string runStatus, double eventsRate);

    ITkChip * GetChip(std::string address);

    private:

    static ItkMonitor * m_me;
    std::string m_partition;
    IPCPartition * m_ipc;
    ISInfoDictionary * m_is;
    std::map<std::string, ISInfo *> m_objs;
};

#endif
